package com.joecy.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * 角色权限表
 */
@Data
public class SecRolePermission extends Model<SecRolePermission> {

    @TableId
    private Integer id;
    private Integer roleId;
    private Integer permissionId;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
