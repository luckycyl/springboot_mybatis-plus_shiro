package com.joecy.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * 权限表
 */
@Data
public class SecPermission extends Model<SecPermission> {

    @TableId
    private Integer id;
    private String name;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
