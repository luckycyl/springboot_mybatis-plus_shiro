package com.joecy.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 用户角色表
 */
@Data
public class SecUserRole extends Model<SecUserRole> {

    @TableId
    private Integer id;
    private Integer userId;
    private Integer roleId;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
