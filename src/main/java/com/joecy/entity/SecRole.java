package com.joecy.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 角色表
 */
@Data
public class SecRole extends Model<SecRole> {

    @TableId
    private Integer id;
    private String name;
    @TableField(exist = false)
    private List<SecPermission> permissions;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
