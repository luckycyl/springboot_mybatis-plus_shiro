package com.joecy.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 用户表
 * 需要继承mybatis-plus的Model<>抽象类
 */
@Data
public class SecUser extends Model<SecUser> {

    //指定id
    @TableId
    private Integer id;
    private String username;
    private String password;
    @TableField(exist = false)
    private List<SecRole> roles;

    //实现默认id方法
    //若一个表没有id 直接return null即可
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
