package com.joecy.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.joecy.entity.SecPermission;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SecPermissionMapper extends BaseMapper<SecPermission> {
}
