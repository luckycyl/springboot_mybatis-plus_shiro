package com.joecy.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.joecy.entity.SecUser;
import com.joecy.entity.SecUserRole;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SecUserRoleMapper extends BaseMapper<SecUserRole> {
}
