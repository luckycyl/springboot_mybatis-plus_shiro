package com.joecy.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.joecy.entity.SecUser;
import org.apache.ibatis.annotations.Mapper;
//必须添加mybatis的@Mapper注解
@Mapper
public interface SecUserMapper extends BaseMapper<SecUser> {
}
