package com.joecy.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.joecy.entity.SecPermission;
import com.joecy.entity.SecRolePermission;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SecRolePermissionMapper extends BaseMapper<SecRolePermission> {
}
