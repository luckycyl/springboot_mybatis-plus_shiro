package com.joecy.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.joecy.entity.SecRole;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SecRoleMapper extends BaseMapper<SecRole> {
}
